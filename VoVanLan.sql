USE [master]
GO
/****** Object:  Database [VoVanLanDB]    Script Date: 06/21/2021 11:56:50 AM ******/
CREATE DATABASE [VoVanLanDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'VoVanLanDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\VoVanLanDB.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'VoVanLanDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\VoVanLanDB_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [VoVanLanDB] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [VoVanLanDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [VoVanLanDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [VoVanLanDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [VoVanLanDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [VoVanLanDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [VoVanLanDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [VoVanLanDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [VoVanLanDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [VoVanLanDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [VoVanLanDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [VoVanLanDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [VoVanLanDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [VoVanLanDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [VoVanLanDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [VoVanLanDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [VoVanLanDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [VoVanLanDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [VoVanLanDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [VoVanLanDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [VoVanLanDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [VoVanLanDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [VoVanLanDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [VoVanLanDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [VoVanLanDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [VoVanLanDB] SET  MULTI_USER 
GO
ALTER DATABASE [VoVanLanDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [VoVanLanDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [VoVanLanDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [VoVanLanDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [VoVanLanDB]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 06/21/2021 11:56:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Description] [text] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Products]    Script Date: 06/21/2021 11:56:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[UnitCost] [decimal](10, 2) NOT NULL,
	[Quantity] [int] NOT NULL,
	[Image] [varchar](255) NOT NULL,
	[Description] [text] NULL,
	[Status] [bit] NOT NULL,
	[Cate_ID] [int] NOT NULL,
 CONSTRAINT [PK__Products__3214EC272878E268] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserAccounts]    Script Date: 06/21/2021 11:56:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserAccounts](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](50) NULL,
	[Name] [nvarchar](50) NULL,
	[Password] [varchar](50) NULL,
	[Status] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([ID], [Name], [Description]) VALUES (1, N'Điện Thoại', N'abc')
INSERT [dbo].[Category] ([ID], [Name], [Description]) VALUES (2, N'LapTop', N'abc')
SET IDENTITY_INSERT [dbo].[Category] OFF
GO
SET IDENTITY_INSERT [dbo].[Products] ON 

INSERT [dbo].[Products] ([ID], [Name], [UnitCost], [Quantity], [Image], [Description], [Status], [Cate_ID]) VALUES (19, N'Laptop MSI Modern 14 B11MOL 420VN i3', CAST(14190000.00 AS Decimal(10, 2)), 100, N'637560644175545559_msi-modern-14-b11mol-420vn-i3-1115g4-xam-ct1.jpg', N'Lorem Ipsum is simply dummy text.', 1, 2)
INSERT [dbo].[Products] ([ID], [Name], [UnitCost], [Quantity], [Image], [Description], [Status], [Cate_ID]) VALUES (22, N'Laptop HP Pavilion x360 14 dw1017TU i3 1115G4', CAST(15990000.00 AS Decimal(10, 2)), 30, N'637513138390120965_hp-pavilion-x360-14-dw-vang-1.jpg', N'Lorem Ipsum is simply dummy text.', 0, 2)
INSERT [dbo].[Products] ([ID], [Name], [UnitCost], [Quantity], [Image], [Description], [Status], [Cate_ID]) VALUES (23, N'Laptop Lenovo Yoga Slim 7 14ITL05 i5 1135G7', CAST(24290000.00 AS Decimal(10, 2)), 70, N'laptop1.jpg', N'Lorem Ipsum is simply dummy text.', 1, 2)
INSERT [dbo].[Products] ([ID], [Name], [UnitCost], [Quantity], [Image], [Description], [Status], [Cate_ID]) VALUES (24, N'Acer Aspire 3 A315-57G-524Z i5-1035G1', CAST(15990000.00 AS Decimal(10, 2)), 100, N'637443973978298927_acer-aspire-3-a315-57g-den-1.jpg', N'Lorem Ipsum is simply dummy text.', 1, 2)
INSERT [dbo].[Products] ([ID], [Name], [UnitCost], [Quantity], [Image], [Description], [Status], [Cate_ID]) VALUES (25, N'Laptop HP Envy 13 ba1028TU i5 1135G7/8GB/512GB/Win 10', CAST(23790000.00 AS Decimal(10, 2)), 100, N'637521082139662322_hp-envy-13-ba-vang-1-evo.jpg', N'Lorem Ipsum is simply dummy text.', 1, 2)
INSERT [dbo].[Products] ([ID], [Name], [UnitCost], [Quantity], [Image], [Description], [Status], [Cate_ID]) VALUES (26, N'ACER Aspire A514-54-540F/i5-1135G7', CAST(17990000.00 AS Decimal(10, 2)), 100, N'637498460861773718_acer-aspire-a514-54-bac-dd.jpg', N'Lorem Ipsum is simply dummy text.', 1, 2)
INSERT [dbo].[Products] ([ID], [Name], [UnitCost], [Quantity], [Image], [Description], [Status], [Cate_ID]) VALUES (27, N'Asus VivoBook M413IA-EK481T/R7-4700U', CAST(18990000.00 AS Decimal(10, 2)), 100, N'637287832326544795_asus-vivobook-m413-bac-dd.jpg', N'Lorem Ipsum is simply dummy text.', 1, 2)
INSERT [dbo].[Products] ([ID], [Name], [UnitCost], [Quantity], [Image], [Description], [Status], [Cate_ID]) VALUES (28, N'Asus Vivobook X515MA-BR074T/N4020', CAST(8490000.00 AS Decimal(10, 2)), 100, N'637502180353045335_asus-vivobook-x515-print-bac-dd.jpg', N'Lorem Ipsum is simply dummy text.', 1, 2)
INSERT [dbo].[Products] ([ID], [Name], [UnitCost], [Quantity], [Image], [Description], [Status], [Cate_ID]) VALUES (29, N'Asus Zenbook UX325EA-EG079T/i5-1135G7', CAST(21690000.00 AS Decimal(10, 2)), 100, N'637492367355006906_asus-zenbook-ux325-xam-dd-bh-2nam.jpg', N'Lorem Ipsum is simply dummy text.', 1, 2)
INSERT [dbo].[Products] ([ID], [Name], [UnitCost], [Quantity], [Image], [Description], [Status], [Cate_ID]) VALUES (30, N'HP 245 G8 R5 3500U', CAST(13890000.00 AS Decimal(10, 2)), 100, N'637520883652622889_hp-245-g8-bac-dd.jpg', N'Lorem Ipsum is simply dummy text.', 0, 2)
INSERT [dbo].[Products] ([ID], [Name], [UnitCost], [Quantity], [Image], [Description], [Status], [Cate_ID]) VALUES (31, N'HP Envy 13 ba1027TU i5 1135G7/2K0B1PA', CAST(21390000.00 AS Decimal(10, 2)), 100, N'637517684143067081_hp-envy-13-ba-vang-dd-icon.jpg', N'Lorem Ipsum is simply dummy text.', 1, 2)
INSERT [dbo].[Products] ([ID], [Name], [UnitCost], [Quantity], [Image], [Description], [Status], [Cate_ID]) VALUES (32, N'iPhone 12 Pro Max 128GB', CAST(29490000.00 AS Decimal(10, 2)), 100, N'637479614485943996_ip-12-pro-max-dd-2nam.jpg', N'Lorem Ipsum is simply dummy text.', 1, 1)
INSERT [dbo].[Products] ([ID], [Name], [UnitCost], [Quantity], [Image], [Description], [Status], [Cate_ID]) VALUES (33, N'Xiaomi POCO X3 Pro NFC 8GB', CAST(7190000.00 AS Decimal(10, 2)), 100, N'637570519716076314_poco-x3-pro-den-dd.jpg', N'Lorem Ipsum is simply dummy text.', 1, 1)
INSERT [dbo].[Products] ([ID], [Name], [UnitCost], [Quantity], [Image], [Description], [Status], [Cate_ID]) VALUES (34, N'Samsung Galaxy Note 20 Ultra', CAST(21990000.00 AS Decimal(10, 2)), 99, N'637322682439532348_ss-note-20-ultra-5g-gold-dd.jpg', N'Lorem Ipsum is simply dummy text.', 1, 1)
INSERT [dbo].[Products] ([ID], [Name], [UnitCost], [Quantity], [Image], [Description], [Status], [Cate_ID]) VALUES (35, N'Nokia 5.4 4GB -128GB', CAST(2990000.00 AS Decimal(10, 2)), 100, N'637445014345646352_nokia-5-4-dd.jpg', N'Lorem Ipsum is simply dummy text.', 1, 1)
INSERT [dbo].[Products] ([ID], [Name], [UnitCost], [Quantity], [Image], [Description], [Status], [Cate_ID]) VALUES (36, N'OPPO A73 6GB-128GB', CAST(4990000.00 AS Decimal(10, 2)), 100, N'637476292243335439_oppo-a73-dd.jpg', N'Lorem Ipsum is simply dummy text.', 1, 1)
INSERT [dbo].[Products] ([ID], [Name], [UnitCost], [Quantity], [Image], [Description], [Status], [Cate_ID]) VALUES (37, N'Samsung Galaxy S21+ 128GB', CAST(17990000.00 AS Decimal(10, 2)), 100, N'637462639794669527_ss-s21-plus-dd.jpg', N'Lorem Ipsum is simply dummy text.', 1, 1)
INSERT [dbo].[Products] ([ID], [Name], [UnitCost], [Quantity], [Image], [Description], [Status], [Cate_ID]) VALUES (38, N'Vivo Y20 4GB - 64GB', CAST(3290000.00 AS Decimal(10, 2)), 100, N'637338790246029486_vivo-y20-trang-dd.jpg', N'Lorem Ipsum is simply dummy text.', 1, 1)
INSERT [dbo].[Products] ([ID], [Name], [UnitCost], [Quantity], [Image], [Description], [Status], [Cate_ID]) VALUES (39, N'Samsung Galaxy A32', CAST(5890000.00 AS Decimal(10, 2)), 100, N'637511350486119197_samsung-a32-xanh-dd.jpg', N'Lorem Ipsum is simply dummy text.', 1, 1)
INSERT [dbo].[Products] ([ID], [Name], [UnitCost], [Quantity], [Image], [Description], [Status], [Cate_ID]) VALUES (40, N'Oppo Reno5 8GB - 128GB', CAST(8290000.00 AS Decimal(10, 2)), 100, N'637445128012092437_oppo-reno5-dd.jpg', N'Lorem Ipsum is simply dummy text.', 1, 1)
INSERT [dbo].[Products] ([ID], [Name], [UnitCost], [Quantity], [Image], [Description], [Status], [Cate_ID]) VALUES (41, N'Samsung Galaxy A52', CAST(9030000.00 AS Decimal(10, 2)), 100, N'637517485966344539_samsung-a52-den-dd.jpg', N'Lorem Ipsum is simply dummy text.', 1, 1)
INSERT [dbo].[Products] ([ID], [Name], [UnitCost], [Quantity], [Image], [Description], [Status], [Cate_ID]) VALUES (42, N'Xiaomi Redmi Note 10 6GB-128GB', CAST(4690000.00 AS Decimal(10, 2)), 100, N'637541695564891790_xiaomi-redmi-note-10-xanh-dd-1.jpg', N'Lorem Ipsum is simply dummy text.', 1, 1)
INSERT [dbo].[Products] ([ID], [Name], [UnitCost], [Quantity], [Image], [Description], [Status], [Cate_ID]) VALUES (43, N'Vsmart Joy 4 3GB-64GB', CAST(2690000.00 AS Decimal(10, 2)), 101, N'637359493077898752_vsmart-joy-4-xanh-dd.jpg', N'Lorem Ipsum is simply dummy text.', 1, 1)
INSERT [dbo].[Products] ([ID], [Name], [UnitCost], [Quantity], [Image], [Description], [Status], [Cate_ID]) VALUES (44, N'OPPO A93 8GB-128GB', CAST(5690000.00 AS Decimal(10, 2)), 100, N'637365326866540463_oppo-A93-dd-1.jpg', N'Lorem Ipsum is simply dummy text.', 1, 1)
SET IDENTITY_INSERT [dbo].[Products] OFF
GO
SET IDENTITY_INSERT [dbo].[UserAccounts] ON 

INSERT [dbo].[UserAccounts] ([ID], [UserName], [Name], [Password], [Status]) VALUES (1, N'Admin', N'Võ Văn Lân', N'admin', N'normal')
INSERT [dbo].[UserAccounts] ([ID], [UserName], [Name], [Password], [Status]) VALUES (3, N'NguyenVanA', N'Nguyễn Văn A', N'abc123!@#', N'bloked')
INSERT [dbo].[UserAccounts] ([ID], [UserName], [Name], [Password], [Status]) VALUES (4, N'TranVanB', N'Trần Văn B', N'abc123!@#', N'normal')
INSERT [dbo].[UserAccounts] ([ID], [UserName], [Name], [Password], [Status]) VALUES (5, N'LeThiNga', N'Lê Thị Nga', N'abc123!@#', N'normal')
INSERT [dbo].[UserAccounts] ([ID], [UserName], [Name], [Password], [Status]) VALUES (6, N'PhanDieuMay', N'Phan Diệu Mây', N'abc123!@#', N'normal')
INSERT [dbo].[UserAccounts] ([ID], [UserName], [Name], [Password], [Status]) VALUES (7, N'LeDinhToan', N'Lê Đình Toàn', N'abc123!@#', N'bloked')
INSERT [dbo].[UserAccounts] ([ID], [UserName], [Name], [Password], [Status]) VALUES (8, N'DinhVanHiep', N'Đinh Văn Hiệp', N'abc123!@#', N'normal')
INSERT [dbo].[UserAccounts] ([ID], [UserName], [Name], [Password], [Status]) VALUES (9, N'DangVietHoang', N'Đặng Việt Hoàng', N'abc123!@#', N'normal')
SET IDENTITY_INSERT [dbo].[UserAccounts] OFF
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [FK__Products__Cate_I__164452B1] FOREIGN KEY([Cate_ID])
REFERENCES [dbo].[Category] ([ID])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [FK__Products__Cate_I__164452B1]
GO
USE [master]
GO
ALTER DATABASE [VoVanLanDB] SET  READ_WRITE 
GO
