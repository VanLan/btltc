﻿using ModelEF.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TestUngDung.Controllers
{
    public class HomeController : Controller
    {
        private HomeDao dao = null;
        public HomeController()
        {
            dao = new HomeDao();
        }
        public ActionResult Index()
        {
            HomeContext model = new HomeContext();
            model.Phone = dao.PhoneListNew();
            model.Laptop = dao.LaptopListNew();
            return View(model);
        }
    }
}