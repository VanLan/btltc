﻿using ModelEF.DAO;
using ModelEF.Model;
using System.IO;
using System.Web;
using System.Web.Mvc;
using TestUngDung.Common;

namespace TestUngDung.Areas.Admin.Controllers
{
    public class ProductController : BaseController
    {
        private ProductDAO dao = null;

        public ProductController()
        {
            dao = new ProductDAO();
        }
        // GET: Admin/Product
        public ActionResult Index(string search, int page = 1, int pagesize = 10)
        {
            var session = Session[Constants.USER_SESSION];
            if (session == null)
            {
                return RedirectToAction("Index", "Login");
            }
            var result = dao.ListSearch(search, page, pagesize);
            ViewBag.Se = search;
            return View(result);
        }

        // GET: Admin/Product/Details/5
        public ActionResult Detail(int id)
        {
            var result = dao.FindID(id);
            return View(result);
        }

        // GET: Admin/Product/Create
        public ActionResult Create()
        {
            SetViewBag();
            return View();
        }

        // POST: Admin/Product/Create
        [HttpPost]
        public ActionResult Create(Product model, HttpPostedFileBase Image)
        {
            try
            {
                SetViewBag();
                if (ModelState.IsValid)
                {
                    if (Image != null && Image.ContentLength > 0)
                    {
                        var result = dao.Insert(model);
                        var fileName = Path.GetFileName(Image.FileName);
                        string path = Path.Combine(Server.MapPath("~/Assets/admin/images/image_SP"), fileName);
                        Image.SaveAs(path);
                        dao.saveIMG(fileName, model.ID);
                        if (!string.IsNullOrEmpty(result))
                        {
                            SetAlert("Tạo mới sản phẩm thành công", "success");
                            return View();
                        }
                        else
                        {
                            SetAlert("Tạo mới sản phẩm không thành công", "error");
                            return View(model);
                        }
                    }
                    else
                    {
                        SetAlert("Uplaod hình ảnh không thành công", "error");
                    }
                }
                return View(model);
            }
            catch
            {
                SetAlert("Thêm Mới Không Thành Công", "warning");
                return View(model);
            }
        }

        // GET: Admin/Product/Edit/5
        public ActionResult Edit(int id)
        {
            SetViewBag();
            var result = dao.FindID(id);
            return View(result);
        }

        // POST: Admin/Product/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Product model, HttpPostedFileBase Image)
        {
            try
            {
                SetViewBag();
                var fileName = "";
                if (model.Image == null)
                {
                    model.Image = dao.FindID(id).Image;
                    fileName = model.Image;
                }
                else
                {
                    if (Image != null && Image.ContentLength > 0)
                    {
                        fileName = Path.GetFileName(Image.FileName);
                        string path = Path.Combine(Server.MapPath("~/Assets/admin/images/image_SP"), fileName);
                        Image.SaveAs(path);

                    }
                    else
                    {
                        SetAlert("Uplaod hình ảnh không thành công", "error");
                    }
                }
                var result = dao.Edit(model);
                dao.saveIMG(fileName, model.ID);
                if (result)
                {
                    SetAlert("Cập nhật sản phẩm thành công", "success");
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    SetAlert("Cập nhật sản phẩm không thành công", "error");
                }
                return RedirectToAction("Index", "Product");
            }
            catch
            {
                return View();
            }
        }
        public void SetViewBag(long? ID = null)
        {
            ViewBag.Cate_ID = new SelectList(dao.ListCategory(), "ID", "Name", ID);
        }
    }
}
