﻿using ModelEF.DAO;
using ModelEF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestUngDung.Common;

namespace TestUngDung.Areas.Admin.Controllers
{
    public class LoginController : Controller
    {
        // GET: Admin/Login
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(UserAccount user)
        {
            if (user.UserName != null)
            {
                if (user.Password != null)
                {
                    var dao = new UserDAO();
                    var result = dao.Login(user.UserName, user.Password);
                    if (result != null)
                    {
                        Session[Constants.USER_SESSION] = result.Name;
                        return RedirectToAction("Index", "Product");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Đăng Nhập Không Thành Công");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Nhập Mật Khẩu Để Đăng Nhập");
                }
            }
            else
            {
                ModelState.AddModelError("", "Nhập Địa Chỉ Email Để Đăng Nhập");
            }
            return View("Index");
        }
    }
}