﻿using ModelEF.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestUngDung.Common;

namespace TestUngDung.Areas.Admin.Controllers
{
    public class UserController : BaseController
    {
        // GET: Admin/User 
        private UserDAO dao = null;
        public UserController()
        {
            dao = new UserDAO();
        }
        public ActionResult Index(string search, int page = 1, int pagesize = 5)
        {
            var session = Session[Constants.USER_SESSION];
            if (session == null)
            {
                return RedirectToAction("Index", "Login");
            }
            var result = dao.ListSearch(search, page, pagesize);
            ViewBag.Se = search;
            return View(result);
        }
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                if (dao.Delete(id))
                {
                    SetAlert("Xoá người dùng thành công", "succces");
                    return View();
                }
                SetAlert("Xoá người dùng không thành công", "error");
                return View();
            }
            catch
            {
                return View();
            }
        }
    }
}
