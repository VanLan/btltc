﻿using ModelEF.Model;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelEF.DAO
{
    public class ProductDAO
    {
        private VoVanLanContext db = null;
        public ProductDAO()
        {
            db = new VoVanLanContext();
        }
        public List<Category> ListCategory()
        {
            return db.Categories.ToList();
        }
        public string Insert(Product entity)
        {
            entity.Name = entity.Name.Trim();
            db.Products.Add(entity);
            db.SaveChanges();
            return entity.Name;
        }
        public IEnumerable<Product> ListSearch(string search, int page, int pagesize)
        {
            IEnumerable<Product> model = db.Products;
            if (!string.IsNullOrEmpty(search))
            {
                return model.Where(x => x.Name.Contains(search)).OrderBy(x => x.Quantity).ThenByDescending(x => x.UnitCost).ToPagedList(page, pagesize);
            }
            return model.OrderBy(x=>x.Quantity).ThenByDescending(x=>x.UnitCost).ToPagedList(page, pagesize);
        }
        public Product FindID(int id)
        {
            return db.Products.Find(id);
        }
        public bool Update(Product entity)
        {
            try
            {
                var result = db.Products.Find(entity.ID);
                db.Entry(result).CurrentValues.SetValues(entity);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Delete(int id)
        {
            try
            {
                var result = db.Products.Find(id);
                db.Products.Remove(result);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public void saveIMG(string filename, int id)
        {
            Product sp = db.Products.FirstOrDefault(x => x.ID == id);
            sp.Image = filename;
            db.SaveChanges();
        }
        public bool Edit(Product entity)
        {
            try
            {
                var result = db.Products.Find(entity.ID);
                db.Entry(result).CurrentValues.SetValues(entity);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
