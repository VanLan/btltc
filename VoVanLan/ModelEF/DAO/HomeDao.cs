﻿using ModelEF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelEF.DAO
{
    public class HomeDao
    {
        private VoVanLanContext db = null;
        public HomeDao()
        {
            db = new VoVanLanContext();
        }
        public List<Product> PhoneListNew()
        {
            return (db.Products.Where(x => x.Cate_ID == 1 && x.Status == true).OrderByDescending(x=>x.ID)).Take(9).ToList();
        }
        public List<Product> LaptopListNew()
        {
            return (db.Products.Where(x => x.Cate_ID == 2 && x.Status == true).OrderByDescending(x => x.ID)).Take(9).ToList();
        }
    }
}
