﻿using ModelEF.Model;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelEF.DAO
{
    public class UserDAO
    {
        private VoVanLanContext db = null;
        public UserDAO()
        {
            db = new VoVanLanContext();
        }
        public UserAccount Login(string username, string password)
        {
            var result = db.UserAccounts.Where(x => x.UserName.Contains("Admin") && x.Password.Contains(password)).FirstOrDefault();
            return result;
        }
        public IEnumerable<UserAccount> ListSearch(string search, int page, int pagesize)
        {
            if (!string.IsNullOrEmpty(search))
            {
                return db.UserAccounts.Where(x => x.Name.Contains(search)).ToPagedList(page, pagesize);
            }
            return db.UserAccounts.OrderBy(x => x.Name).ToPagedList(page, pagesize);
        }
        public bool Delete(int id)
        {
            try
            {
                var result = db.UserAccounts.Find(id);
                if (result.Status != "blocked")
                {
                    return false;
                }
                db.UserAccounts.Remove(result);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
