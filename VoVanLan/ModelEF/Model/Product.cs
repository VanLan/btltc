﻿namespace ModelEF.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Product
    {
        public Product()
        {
            Description = "Lorem Ipsum is simply dummy text.";
        }
        [Display(Name ="Mã Sản Phẩm")]
        public int ID { get; set; }

        [Required]
        [StringLength(255)]
        [Display(Name = "Tên Sản Phẩm")]
        public string Name { get; set; }
        [DisplayFormat(DataFormatString = "{0:0,00}")]
        [Display(Name = "Giá Bán")]
        public decimal UnitCost { get; set; }
        [Display(Name = "Số Lượng")]
        public int Quantity { get; set; }

        [Required]
        [StringLength(255)]
        [Display(Name = "Ảnh Đại Diện")]
        public string Image { get; set; }

        [Column(TypeName = "text")]
        [Display(Name = "Mô Tả")]
        public string Description { get; set; }
        [Display(Name = "Trạng Thái")]
        public bool Status { get; set; }
        [Display(Name = "Tên Thể Loại")]
        public int Cate_ID { get; set; }

        public virtual Category Category { get; set; }
    }
}
