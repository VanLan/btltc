﻿namespace ModelEF.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class UserAccount
    {
        public int ID { get; set; }

        [StringLength(50)]
        [Display(Name = "Username")]
        public string UserName { get; set; }

        [StringLength(50)]
        [Display(Name = "Họ Và Tên")]
        public string Name { get; set; }

        [StringLength(50)]
        [Display(Name = "Mật Khẩu")]
        public string Password { get; set; }

        [StringLength(100)]
        [Display(Name = "Trạng Thái")]
        public string Status { get; set; }
    }
}
